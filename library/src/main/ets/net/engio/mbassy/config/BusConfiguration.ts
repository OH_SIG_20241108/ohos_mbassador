/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { IBusConfiguration } from './IBusConfiguration'
import { Feature } from './Feature'
import type { IPublicationErrorHandler } from '../bus/error/IPublicationErrorHandler'
import { Utils } from '../utils/Utils'

export class BusConfiguration implements IBusConfiguration {
    private properties: Map<String, Object> = new Map<String, Object>();
    private publicationErrorHandlers: Array<IPublicationErrorHandler> = new Array<IPublicationErrorHandler>()

    addFeature(feature: Feature): IBusConfiguration {
        let className = Utils.getClassName(feature);
        this.properties.set(className, feature);
        return this;
    }

    getFeature(name: String): Feature {
        if (this.properties.has(name)) {
            let value = this.properties.get(name)
            return value;
        }
        return undefined;
    }

    addPublicationErrorHandler(handle: IPublicationErrorHandler) {
        if (handle) {
            this.publicationErrorHandlers.push(handle);
        }
        return this;
    }

    getRegisteredPublicationErrorHandlers(): Array<IPublicationErrorHandler> {
        return this.publicationErrorHandlers;
    }
}