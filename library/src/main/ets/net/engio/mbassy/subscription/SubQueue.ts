/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Queue from '@ohos.util.Queue';
import { Subscription } from './Subscription';
import ArrayList from '@ohos.util.ArrayList';

export class SubQueue {
    private static sInstance: SubQueue;
    private subQueue: Queue<Subscription> = new Queue<Subscription>();

    private constructor() {
    }

    static getInstance(): SubQueue {
        if (!SubQueue.sInstance) {
            SubQueue.sInstance = new SubQueue();
        }
        return SubQueue.sInstance;
    }

    getSubQueue(): Queue<Subscription> {
        return this.subQueue;
    }

    addSub(sub: Subscription) {
        this.subQueue.add(sub);
    }

    public addAllSub(subscriptions: ArrayList<Subscription>) {
        if (!subscriptions || subscriptions.length == 0) {
            return;
        }
        subscriptions.forEach((v, i) => {
            this.subQueue.add(v);
        })
    }
}